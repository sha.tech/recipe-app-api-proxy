# Recipe App API Proxy 

NGINX proxy app for recipe app API [ HomeBaked :https://github.com/Shahna-C/recipe-app-api ]

## Usage

### Environment Variables 

* `LISTEN_PORT` - Port to listen on (default: `8000`)
* `APP_HOST` - Hostname of the app to forward requests to (default: `app`)
* `APP_PORT` - Port of the app to forward requests to (default: `9000`)

<h2>Built With</h2>
  <ul>
    <li>Nginx
    <li>GitHub
    <li>Docker
    <li>Terraform
    <li>Python 3
    <li>AWS : ECR, EC2, AWS-Vault, ALB, ECS, VPC, S3
  </ul>

<h2>Authors</h2>
<p>Shahna Campbell</p>

<h2>Acknowledgments</h2>
<p>Made possible by DevOps Deployment Automation with Terraform, AWS, and Docker course on Udemy by Mark Winterbottom</p>